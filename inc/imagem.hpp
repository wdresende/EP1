#ifndef IMAGEM_H
#define IMAGEM_H
#include <string>

using namespace std;

struct Pixel {
   int red;
   int green;
   int blue;
};

class Imagem {
private:

   string formato;
   int largura;
   int altura;
   int cor_max
   Pixel pixel [altura][largura];
public:
   Imagem();
   ~Imagem();
   void setLargura();
   void setAltura();
   void setPixel(int linha, int coluna);
   void setCorMAx();
   void setFormato();

   string getFormato(string formato);
   int getCorMAx(int cor_max);
   int getLargura(int largura);
   int getAltura(int altura);
   Pixel getPixel(int linha, int coluna);

   void abrirImag();
   void copiaImag();

};

#endif 
