#include "imagem.hpp"


Imagem::Imagem(){
  
   altura = 0;
   largura = 0;
   cor_max = 0;
   formato = "";
}

Imagem::~Imagem(){

}

int Imagem::getLargura(){
   return largura;
}

void Imagem::setLargura(int largura){
   this->largura = largura;
}

int Imagem::getAltura(){
   return altura;
}

void Imagem::setAltura(int altura){
   this->altura = altura;
}

int Imagem::getCorMax(){
   return cor_max;
}

void Imagem::getCorMax(int cor_max){
   this->cor_max = cor_max;
}

string Imagem::getFormato(){
   return formato;
}

void Imagem::setFormato(string formato){
   this->formato = formato;
}

